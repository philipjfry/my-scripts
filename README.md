My Scripts

Housekeeping:

I created this one as a way to keep my home directory cleaned up.  The functionality is limited, but I'm hoping to add
some real cool stuff to it later like cloud storage syncing and expiration dates for files in the Downloads/Documents
directories.

Housekeeping Configs:
 - Home
 - Documents
 - Images
 - Destination for Documents
 - Destination for Images

These are required for the script to function properly.  Edit the logging configs at your own risk.