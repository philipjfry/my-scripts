#!/usr/bin/env python3

import os
import logging
import logging.config
import shutil
import yaml

# Loading in the configurations
with open('config/myscripts_config.yaml', 'r') as main_config:
    config = yaml.safe_load(main_config)

# Setting up logging
logging.config.dictConfig(config['logging'])
logger = logging.getLogger("Housekeeping")

# Setting up housekeeping configs
housekeeper_conf = config['housekeeping']

def housekeeping(file_types: list = None, source: str = None, destination: str = None) -> None:
    # Check the fields:
    for field in file_types, source, destination:
        if field is None:
            logger.error(f'{field} is empty.  This is a required field and can not be a null value.')
            exit(1)

    os.chdir(source)
    filelist = os.listdir(source)

    for file in filelist:
        for file_ext in file_types:
            if file_ext in file:
                if not os.path.isfile(f'{destination}/{file}'):
                    try:
                        logger.info(f'Moving {file} to {destination}')
                        shutil.move(file, destination)
                    except IOError:
                        logger.error(f'Unable to move {file} to {destination}', exc_info=True)
                else:
                    #TODO: Allow a force option to be applied to force file overwrite
                    logger.warning(f'{file}: A file by this name already exists at {destination}.  Will not force the move.')


def main() -> None:
    # Establishing source
    home = housekeeper_conf["home"]

    # Establishing destinations
    docs_dest_location = f'{housekeeper_conf["home"]}/{housekeeper_conf["documents"]}'
    images_dest_location = f'{housekeeper_conf["home"]}/{housekeeper_conf["images"]}'

    # Establishing file types lists
    docs_file_types = housekeeper_conf['docs_formats']
    images_file_types = housekeeper_conf['images_formats']

    # Starting housekeeping functions for each of the file types to their respective destinations
    logger.info('Starting Cleanup')

    # Moving all document file types to the specified destination
    logger.info(f'Checking for files destined to {housekeeper_conf["documents"]}')
    housekeeping(docs_file_types, home, docs_dest_location)

    # Moving all image file types to the specified destination
    logger.info(f'Checking for files destined to {housekeeper_conf["images"]}')
    housekeeping(images_file_types, home, images_dest_location)

    # Completion
    logger.info('Completed Cleanup')
    exit(0)

if __name__ == "__main__":
    main()