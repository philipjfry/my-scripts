PLEX_USER='plex'
PLEX_DIR='/etc/plex'
PLEX_DATA_DIR='/mnt/plex_data'

function set_plex_dir_perm_own() {
    current_dir_owner=$(stat -c '%U' $PLEX_DIR)
    if [ $current_dir_owner != 'plex' ]; then
        echo "Current ownership and permissions not set to plex user. Fixing..."
        chown -R $PLEX_USER:$PLEX_USER $PLEX_DIR
        chmod -R 755 $PLEX_DIR
    else
        echo "Permissions and ownership set correctly"
    fi
}

function check_config_directory() {
    if [ -d "$PLEX_DIR/config" ]; then
        echo "Plex config directory exists"
    else
        echo "Plex config directory does not exist. Creating..."
        mkdir -p /etc/plex/config
        check_config_directory
    fi
}

function check_data_directory() {
    if [ -d "$PLEX_DATA_DIR" ]; then
        echo "Plex data directory exists"
    else
        echo "Plex data directory does not exist. Creating..."
        mkdir -p /mnt/plex_data
    fi
}

function update_and_install_podman_extension_for_cockpit() {
    dnf update -y
    dnf install -y cockpit-podman
    systemctl enable --now podman
}

function check_transcode_directory() {
    if [ -d "$PLEX_DIR/transcode" ]; then
        echo "Plex transcode directory exists"
    else
        echo "Plex transcode directory does not exist. Creating..."
        mkdir -p /etc/plex/transcode
        check_transcode_directory
    fi
}

function set_selinux_permissive() {
    sed -i 's/SELINUX\=enforcing/SELINUX\=permissive/' /etc/selinux/config
    echo "++++++++++ REBOOT WILL BE REQUIRED +++++++++++"
}

function check_selinux_status() {
    current_status=$(sestatus | grep Current | awk '{print $3}')
    if [ $current_status != 'permissive' ]; then
        echo "SELinux currently set to enforcing.  Changing to permissive"
        set_selinux_permissive
    else
        echo "SELinux correctly set to permissive"
    fi
}

function check_firewall_status() {
    firewall_state=$(firewall-cmd --state)
    if [ $firewall_state == 'running' ]; then
        echo "Firewalld is running.  Stopping and disabling..."
        systemctl disable --now firewalld
    else
        echo "Firewalld is not running"
    fi
}



function print_summary() {
    echo "Basic setup complete!"
    echo "Please reboot as soon as possible to pick up all changes!"
    echo " "
    echo "+====================================================================+"
    echo "                          FOR USE IN PODMAN                           "
    echo "+====================================================================+"
    echo "PORT MAPPING:"
    echo "  IP ADDRESS: Leave this field blank"
    echo "  PORT:"
    echo "    - Host port: 32400"
    echo "    - Container port: 32400"
    echo "VOLUMES:"
    echo "  Transcode:"
    echo "    - Host Path: $PLEX_DIR/transcode"
    echo "    - Container Path: /transcode"
    echo "  Config (Database):"
    echo "    - Host Path: $PLEX_DIR/config"
    echo "    - Container Path: /config"
    echo "  Data (all the media files)"
    echo "    - Host Path: /mnt/plex_data"
    echo "    - Contaienr Path: /data"
    echo "ENVIRONMENT VARIABLES:"
    echo "  Timezone:"
    echo "    - Key: TZ"
    echo "    - Value: America/New_York"
    echo "  Plex Claim Code (link to your Plex Pass):"
    echo "    - Key: PLEX_CLAIM"
    echo "    - Value: <insert code from plex.tv/claim>"
}

check_config_directory
check_transcode_directory
set_plex_dir_perm_own
check_selinux_status
check_firewall_status
update_and_install_podman_extension_for_cockpit
print_summary